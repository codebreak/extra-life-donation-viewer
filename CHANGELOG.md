# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.1.1 - 2022-10-13

### Added

- Added the ability to change the output format.
- More information inside the help page.

### Removed

- Custom font from Google Fonts.

## 0.1.0 - 2021-11-22

### Added

- Initial implementation of the web page.
- Display help when no url params are provided.
- Can display donation of a participant when provided with a participant id.
- Can display donation of a team when provided with a team id.
- Can change update delay using an url param.