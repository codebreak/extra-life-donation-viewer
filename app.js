'use strict';

const DONATIONS_FORMAT_PLACEHOLDER = "@DONATIONS";
const DEFAULT_FORMAT = `${DONATIONS_FORMAT_PLACEHOLDER} $`;
const DEFAULT_UPDATE_DELAY = 60 * 1000; // Every 60 seconds.

const appTag = document.getElementById("app");
const helpTag = document.getElementById("help");

const params = new URLSearchParams(window.location.search);
const participantId = params.get("participantId");
const teamId = params.get("teamId");
const format = params.get("format") || DEFAULT_FORMAT;
const updateDelay = params.get("updateDelay") || DEFAULT_UPDATE_DELAY;

if (participantId || teamId) {
    helpTag.parentElement.removeChild(helpTag);

    let url;
    if (participantId) {
        url = `https://extra-life.org/api/participants/${participantId}`;
    } else {
        url = `https://extra-life.org/api/teams/${teamId}`;
    }

    function fetchDonations() {
        fetch(url).then(response => {
            if (response.status === 200) {
                response.json().then(value => {
                    appTag.innerText = format.replace(DONATIONS_FORMAT_PLACEHOLDER,value.sumDonations);
                    setTimeout(fetchDonations, updateDelay);
                });
            }
        }); 
    }
    
    fetchDonations();
}