# Extra-Life Donation Viewer

Displays the donations of a participant or a team.

## Usage

See [the documentation](https://codebreak.gitlab.io/extra-life-donation-viewer).

## Changelog

See the [CHANGELOG.md](CHANGELOG.md) file for version details.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details